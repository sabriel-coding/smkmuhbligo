<?php

class Connection {
    private $hostname = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "smkmuhbligo";
    function getConnection() {
        return mysqli_connect($this->hostname,$this->username,$this->password,$this->database);
    }
}
class ProfilSekolah {
    var $nama_sekolah = "SMK MUHAMMADIYAH BLIGO";
    var $alamat_sekolah = [
        'nama_jalan' => 'Jl. Sapugarut Gg. 7',
        'nama_kecamatan' => 'Kec. Buaran',
        'nama_kota' => 'Pekalongan',
        'kode_pos' => 51174
    ];
}
class VariableUmum {
    var $namaLengkapJurusan = [
        'AKL' => 'Akuntansi dan Keuangan Lembaga',
        'FKK' => 'Farmasi Klinis dan Komunitas',
        'TKJ' => 'Teknik Komputer dan Jaringan',
        'TKR' => 'Teknik Kendaraan Ringan Otomotif',
        'TBSM' => 'Teknik dan Bisnis Sepeda Motor'
    ];
}