<?php
include 'config.php';
$connect = new Connection(); $connect = $connect->getConnection();


class Actions {
    function checkAction() {
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'logout': {
                    $this->logout();
                }
                case 'delete': {
                    if (isset($_GET['nis']) AND isset($_GET['on'])) {
                        $this->delete();
                    }
                }
            }
        }
    }
    function logout() {
        global $_SESSION;
        unset($_SESSION['nis'],$_SESSION['nama'],$_SESSION['jurusan'],$_SESSION['kelas']);
        echo "<div class='alert alert-success'>Terimakasih Telah Berkunjung</div>";
        header("refresh: 2; url=/smkmuhbligo/index.php");
    }
    function delete() {
        global $_GET, $connect;
        $result = mysqli_query($connect,"DELETE FROM {$_GET['on']} WHERE nis={$_GET['nis']}");
        return $result;
    }
    function edit() {
        header('location: views/edit.php');
    }
}
class Functions {
    function getResult($query,$ambil) {
        global $connect;
        $result = mysqli_fetch_assoc(mysqli_query($connect, $query));
        return $result[$ambil];
    }
}
class Index extends Functions {
    function getDataUser() {
        global $_SESSION, $connect;
        if(isset($_SESSION['nis'])) {
            $nis = $_SESSION['nis'];
            $result = mysqli_query($connect, "SELECT * FROM tb_siswa WHERE nis=$nis");
            $result = mysqli_fetch_assoc($result);
            $_SESSION['nama'] = $result['nama'];
            $_SESSION['jurusan'] = $result['jurusan'];
            $_SESSION['kelas'] = $result['kelas'];
            $_SESSION['no_absen'] = $result['no_absen'];
        }
    }
}
class LoginToAbsen {
    function loginToAbsen() {
        global $connect;
        $result = mysqli_query($connect, "SELECT wush FROM tb_absensi WHERE nis={$_GET['nis']}");
        $result = mysqli_fetch_assoc($result);
        if ($result['wush'] == 1) $result = 0; else $result = 1;
        $result1 = mysqli_query($connect, "UPDATE tb_absensi SET wush=$result WHERE nis={$_GET['nis']}");
        echo "UPDATE tb_absensi SET wush=$result WHERE nis={$_GET['nis']}";
        if ($result1) {
            $result2 = mysqli_query($connect,"SELECT tanggal FROM tb_absensi WHERE nis={$_GET['nis']}");
            $result = mysqli_fetch_assoc($result2);
            $hasil = strtotime($result['tanggal']);
            $day = date('d',$hasil);
            $month = date('m',$hasil);
            $month = $this->getMonth($month);
            $query3 = "UPDATE tb_absensi SET `$day`=1,bulan='$month' WHERE nis={$_GET['nis']}";    
            echo $query3 . "<br>";
            $result3 = mysqli_query($connect,$query3);
            if ($result3) {return true;} else return false;
        } else return false;
    }
    function getMonth($bulan) {
        switch ($bulan) {
            case 1: return 'Januari';break;
            case 2: return 'Februari';break;
            case 3: return 'Maret';break;
            case 4: return 'April';break;
            case 5: return 'Mei';break;
            case 6: return 'Juni';break;
            case 7: return 'Juli';break;
            case 8: return 'Agustus';break;
            case 9: return 'September';break;
            case 10: return 'Oktober';break;
            case 11: return 'November';break;
            case 12: return 'Desember';break;
        }
    }
}