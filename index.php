<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | SMK Muhammadiyah Bligo</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
</head>
<body class="bg-light">
    <?php include 'functions.php';
    if (isset($_SESSION['nis'])) {
        header('location: home.php');
    } else include 'qrscanner.php';
    if (isset($_GET['nis']) AND !isset($_SESSION['nis'])) {
        $_SESSION['nis'] = $_GET['nis'];
        $func = new LoginToAbsen();
        $result = $func->loginToAbsen();
        var_dump($result);
        header('location: home.php');
    } else include 'qrscanner.php';
    ?>
</body>
</html>