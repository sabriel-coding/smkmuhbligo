<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | SMK Muhammadiyah Bligo</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
</head>
<body class="bg-light" style="min-width: min-content">
    <?php include_once('functions.php');include_once('config.php');
    if(isset($_GET['action'])) {
        $action = new Actions();
        $action->checkAction();
    }
    if(isset($_GET['page'])) {include "views/{$_GET['page']}.php";}
    else include 'views/index.php';
    ?>
</body>
</html>