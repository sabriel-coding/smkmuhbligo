<?php
include "../config.php";
$connect = new Connection();
$connect = $connect->getConnection();


class Absensi {
    function checkDisable($type) {
        global $_POST;
        if(!isset($_POST[$type]) AND empty($_POST[$type])) echo "disabled";
    }
    function getValue($type) {
        global $_POST;
        if(isset($_POST[$type]) AND !empty($_POST[$type])) echo "value='{$_POST[$type]}'";
    }
    function printOption($select = null,...$params) {
        foreach($params as $param) {
            if(!empty($select) AND $param == $select) {
                echo "<option value='$param' selected>$param</option>";
                continue;
            } else echo "<option value='$param'>$param</option>";
        }
    }
}