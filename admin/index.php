<?php session_start();
include_once('../config.php');
if(isset($_SESSION['smkmuhbligo'])) {
    var_dump($_SESSION);
    header("location: admin.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin | SMK Muhammadiyah Bligo</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
</head>

<body class="bg-light">
    <div class="container-fluid d-flex align-items-center justify-content-center" style="width: 100vw; height: 80vh">
        <div class="bg-white px-5 shadow">
            <h2 class="text-center m-lg-5">Login Administrator</h2>
            <form action="" method="post">
                <div class="row form-group">
                    <label class="col-sm-4 col-form-label">Username</label>
                    <input type="text" name="username" class="col-sm-8 form-control" autofocus>
                </div>
                <div class="row form-group">
                    <label class="col-sm-4 col-form-label">Password</label>
                    <input type="password" name="user_password" class="col-sm-8 form-control">
                </div>
                <div class="row form-group text-center">
                    <div class="col-sm-12">
                        
                        <input type="submit" value="Login" name="login" class="btn btn-primary px-4 mt-2">
                    </div>
                </div>
            </form>
            <?php
            if (isset($_POST['login'])) {
                $connect = new Connection;
                $connect = $connect->getConnection();
                $query = "SELECT * FROM ms_users WHERE username='$_POST[username]' AND user_password='$_POST[user_password]'";
                $result = mysqli_num_rows(mysqli_query($connect, $query));
                echo "<div class='w-100'>";
                if ($result) {
                    echo "<div class='alert alert-success'>Selamat Datang Admin</div>";
                    $_SESSION['smkmuhbligo'] = true;
                    header("refresh: 2; url=admin.php");
                } else echo "<div class='alert alert-danger'>Login Gagal</div>";
                echo "</div>";
            }
            ?>
        </div>
    </div>
</body>

</html>