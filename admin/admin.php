<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administrator | SMK Muhammadiyah Bligo</title>
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <style>
        .btn-group-vertical>a {
            text-align: left;
            border: 1px solid #6c757d;
            border-left: 0;
            padding: 12px 16px;
        }
    </style>
</head>

<body class="bg-light">
    <div class="w-100 d-flex">
        <div class="text-white" style="flex: 1.2">
            <div class="bg-dark h-100 position-relative">
                <h3 class=" text-center p-3 mb-0 py-4 bg-dark">Administrator</h3>
                <div class="btn-group-vertical w-100">
                    <a href="admin.php" class="btn btn-dark">Home</a>
                    <a href="?page=daftar_siswa" class="btn btn-dark">Daftar Siswa</a>
                    <a href="?page=tambah_siswa" class="btn btn-dark">Tambah Siswa</a>
                    <a href="?page=daftar_absensi" class="btn btn-dark">Daftar Absensi</a>
                    <a href="?page=tambah_absensi" class="btn btn-dark">Tambah Absensi</a>
                </div>
                <a href="?session=logout" class="btn btn-danger w-100 position-absolute p-2" style="bottom:0; left:0; background-color: #F00;">Logout</a>
            </div>
        </div>
        <div class="container-fluid pt-3" style="flex: 5; height: 100vh; overflow: auto;">
            <?php include 'functions.php';
            isset($_GET['page']) ? include "views/{$_GET['page']}.php" : '';
            if (isset($_GET['action']) AND isset($_GET['nis'])) {
                $actions = new Actions();
                $actions = $actions->checkAction();
            }
            if (isset($_GET['session']) AND $_GET['session'] == 'logout') {
                unset($_SESSION['smkmuhbligo']);
                echo "<div class='alert alert-success text-center'>Good Bye Admin :)</div>";
                header("refresh: 2; url=index.php");
            }
            ?>
        </div>
    </div>
</body>

</html>