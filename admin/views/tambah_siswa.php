<?php $tambah = new TambahSiswa(); ?>
<h1 class="text-center">Tambah Data Siswa</h1>
<hr class="m-2">
<?php $tambah->insertTambahSiswa('tambah_submit','daftar_siswa') ?>
<form action="" method="post" class="w-75 m-auto">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">NIS</label>
        <div class="col-sm-9">
            <input type="number" name="nis" class="form-control" onchange="this.form.submit()" value="<?php echo $tambah->getValue('nis') ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Nama</label>
        <div class="col-sm-9">
            <input type="text" name="nama" class="form-control" value="<?php echo $tambah->getValue('nama'); ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Jurusan</label>
        <div class="col-sm-9">
            <select name="jurusan" class="form-control" <?php echo empty($_POST['nis']) ? 'disabled' : '' ?>>
                <?php $tambah->printOptionJurusan() ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Kelas</label>
        <div class="col-sm-9">
            <select name="kelas" class="form-control" <?php echo empty($_POST['nis']) ? 'disabled' : '' ?>>
                <?php $tambah->printOptionKelas() ?>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">No. Absen</label>
        <div class="col-sm-9">
            <input type="number" name="no_absen" class="form-control" <?php echo empty($_POST['nis']) ? 'disabled' : '' ?> value="<?php echo $tambah->getValue('no_absen') ?>">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Kelompok</label>
        <div class="col-sm-9">
            <select name="kelompok" class="form-control" <?php echo empty($_POST['nis']) ? 'disabled' : '' ?>>
                <?php echo empty($_POST['nis'])?'<option value></option>':''; ?>
                <?php $tambah->printOption($tambah->getValue('kelompok'),...$tambah->kelompok) ?>
            </select>
        </div>
    </div>
    <div class="form-group row col-sm-12 text-center">
        <div class="col-sm-12 text-center">
            <input type="submit" name="tambah_submit" value="Tambah" class="btn btn-dark px-3">
        </div>
    </div>
</form>