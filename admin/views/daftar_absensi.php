<?php $absensi = new Absensi(); ?>
<h1 class="text-center">Absensi</h1>
<hr>
<form action="" method="POST">
    <div class="w-25" style="transform: scale(0.8)">
        <div class="row form-group my-1">
            <label class="col-4 col-form-label">Jurusan</label>
            <div class="col-sm-8">
                <select name="ftr_jurusan" class="form-control" id="ftr_jurusan" onchange="this.form.submit()">
                    <option value>All</option>
                    <?php $absensi->printOption(isset($_POST['ftr_jurusan']) ? $_POST['ftr_jurusan'] : null, ...$jurusan) ?>
                </select>
            </div>
        </div>
        <div class="row form-group my-1">
            <label class="col-sm-4 col-form-label">Kelas</label>
            <div class="col-sm-8">
                <select name="ftr_kelas" class="form-control" id="ftr_kelas" <?php echo !empty($_POST['ftr_jurusan']) ?: 'disabled'; ?> onchange="this.form.submit()">
                    <option value>All</option>
                    <?php $absensi->printOption(!empty($_POST['ftr_kelas']) ? $_POST['ftr_kelas'] : null, ...$kelas) ?>
                </select>
            </div>
        </div>
    </div>
    <table class="w-100">
        <div class="row" style="transform: scale(0.8)">
            <div class="col form-group row">
                <label class="col-sm-3 col-form-label">View</label>
                <div class="col-sm-9">
                    <input type="number" name="ftr_views" class="form-control" onchange="this.form.submit()" value="<?php $absensi->getValue('ftr_views') ?>">
                </div>
            </div>
            <div class="col form-group row">
                <label class="col-sm-3 col-form-label">Bulan</label>
                <div class="col-sm-9">
                    <select name="ftr_bulan" class="form-control" onchange="this.form.submit()">
                        <option value>All</option>
                        <?php $absensi->printOption(!empty($_POST['ftr_bulan']) ? $_POST['ftr_bulan'] : null, ...$absensi->bulan) ?>
                    </select>
                </div>
            </div>
            <div class="col form-group row">
                <label class="col-sm-3 col-form-label">Tahun</label>
                <div class="col-sm-9">
                    <select name="ftr_tahun" class="form-control" onchange="this.form.submit()">
                        <option value>All</option>
                        <?php $absensi->printOption(!empty($_POST['ftr_tahun']) ? $_POST['ftr_tahun'] : null, 2018, 2019, 2020, 2021) ?>
                    </select>
                </div>
            </div>
            <div class="col form-group row">
                <label class="col-sm-3 col-form-label">Search</label>
                <div class="col-sm-9">
                    <input type="search" name="ftr_search" class="form-control" onkeyup="form.this.submit()" value="<?php echo $absensi->getValue('ftr_search') ?>">
                </div>
            </div>
            <div class="col-sm-1 form-group row">
                <input type="submit" value="Search" class="btn btn-dark">
            </div>
        </div>
    </table>
</form>
<fieldset style="overflow: auto;">
    <table class="table">
        <thead class="bg-dark table text-white">
            <?php $days = $absensi->getDay();
            $absensi->printTh('NIS', 'Nama', 'Kelas', ...$days);
            $dayo = $absensi->dayToString();
            ?>
        </thead>
        <?php $query = $absensi->getQuery('tb_absensi.nis,nama,jurusan,kelas' . $dayo, 'tb_siswa INNER JOIN tb_absensi ON tb_siswa.nis = tb_absensi.nis', 'tb_absensi.nis', 'nama');
        $results = mysqli_query($connect, $query);
        while ($result = mysqli_fetch_assoc($results)) {
            echo "<tr>";
            echo "<td>{$result['nis']}</td>";
            echo "<td>{$result['nama']}</td>";
            echo "<td>{$result['kelas']}</td>";
            $absensi->printTd($result);
            echo "</tr>";
        }
        ?>
    </table>
</fieldset>
</div>