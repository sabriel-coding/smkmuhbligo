<?php $tambah = new TambahForm(); ?>
<h1 class="text-center">Tambah Absensi</h1>
<hr>
<form action="" method="post" class="w-75 m-auto">
    <?php $tambah->insertForm('tambah_submit', 'daftar_absensi', 'tb_siswa') ?>
    <div class="row form-group">
        <label class="col-sm-4 col-form-label">NIS</label>
        <div class="col-sm-8">
            <input type="number" name="nis" class="form-control" onchange="this.form.submit()" value="<?php echo $tambah->getValue('nis') ?>">
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-4 col-form-label">Nama</label>
        <div class="col-sm-8">
            <input type="text" name="nama" class="form-control" <?php $kon = $tambah->printValueFromNIS('nama') ?> disabled>
            <?php if (isset($_POST['nis']) and $kon == false) echo "<div class='alert alert-danger p-2'>NIS Tidak Ditemukan</div>" ?>
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-4 col-form-label">Jurusan</label>
        <div class="col-sm-8">
            <input type="text" name="jurusan" class="form-control" <?php $kon = $tambah->printValueFromNIS('jurusan') ?> disabled>
            <?php if (isset($_POST['nis']) and $kon == false) echo "<div class='alert alert-danger p-2'>NIS Tidak Ditemukan</div>" ?>
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-4 col-form-label">Kelas</label>
        <div class="col-sm-8">
            <input type="text" name="kelas" class="form-control" <?php $kon = $tambah->printValueFromNIS('kelas') ?> disabled>
            <?php if (isset($_POST['nis']) and $kon == false) echo "<div class='alert alert-danger p-2'>NIS Tidak Ditemukan</div>" ?>
        </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-4 col-form-label">Tanggal</label>
        <div class="col-sm-8">
            <input type="date" name="tanggal" id="tanggal" class="form-control">
        </div>
    </div>
    <div class="row form-group text-center">
        <div class="text-center m-auto">
            <input type="submit" value="Tambah" name="tambah_submit" class="form-control btn btn-dark px-4">
        </div>
    </div>
</form>