<?php $daftar = new DaftarSiswa() ?>
<h1 class="text-center">Daftar Siswa</h1>
<hr class="m-2">
<form action="" method="post" style="transform: scale(0.8)">
    <div class="w-25">
        <div class="row form-group my-1">
            <label class="col-4 col-form-label">Jurusan</label>
            <div class="col-sm-8">
                <select name="ftr_jurusan" class="form-control" onchange="this.form.submit()">
                    <option value>All</option>
                    <?php $daftar->printOption($daftar->getValue('ftr_jurusan'), ...$daftar->jurusan) ?>
                </select>
            </div>
        </div>
        <div class="row form-group my-1">
            <label class="col-sm-4 col-form-label">Kelas</label>
            <div class="col-sm-8">
                <select name="ftr_kelas" class="form-control" id="ftr_kelas" <?php echo !empty($_POST['ftr_jurusan']) ?: 'disabled'; ?> onchange="this.form.submit()">
                    <option value>All</option>
                    <?php $daftar->printOptionKelas(isset($_POST['ftr_kelas']) ? $_POST['ftr_kelas'] : null, 'ftr_jurusan')
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="d-flex" style="justify-content:flex-end">
        <div class="row form-group" style="width: auto">
            <label class="col-sm-3 col-form-label">Search</label>
            <div class="col-sm-6">
                <input type="text" name="ftr_search" class="form-control" value="<?php echo $daftar->getValue('ftr_search') ?>">
            </div>
            <input type="submit" value="Search" class="btn btn-dark">
        </div>
    </div>
</form>
<fieldset style="overflow: auto;">
    <table class="w-100 table">
        <thead class="bg-dark text-white">
            <?php $daftar->printTh('NIS', 'Nama', 'Jurusan', 'Kelas', 'No. Absen', 'Kelompok','Action'); ?>
        </thead>
        <?php $query = $daftar->getQuery('*','tb_siswa');
        $results = mysqli_query($connect, $query);
        $daftar->printTd($_GET['page'],$results, 'nis', 'nama', 'jurusan', 'kelas', 'no_absen', 'kelompok');
        ?>
    </table>
</fieldset>