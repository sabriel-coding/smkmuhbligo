<?php $edit = new EditForm();
if ($_GET['on'] == 'daftar_siswa') { ?>
    <h1 class="text-center">Tambah Data Siswa</h1>
    <hr class="m-2">
    <?php $return = $edit->updateForm('edit_submit');
    $edit->printAlert($return,'Berhasil Mengubah Data','Gagal Mengubah Data');
    $results = $edit->getDataFromNIS();
    ?>
    <form action="" method="post" class="w-75 m-auto">
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">NIS</label>
            <div class="col-sm-9">
                <input type="number" name="nis" class="form-control" value="<?php echo $results['nis'] ?>" disabled>
            </div>
            <label class="col-sm-3 col-form-label text-warning">Edit NIS =></label>
            <div class="col-sm-9">
                <input type="number" name="nis" class="form-control" value="<?php echo $results['nis'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Nama</label>
            <div class="col-sm-9">
                <input type="text" name="nama" class="form-control" value="<?php echo $results['nama']; ?>" disabled>
            </div>
            <div class="col-sm-3 col-form-label text-warning">Edit Nama =></div>
            <div class="col-sm-9">
                <input type="text" name="nama" class="form-control" value="<?php echo $results['nama'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Jurusan</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $results['jurusan'] ?>" disabled>
            </div>
            <label class="col-sm-3 col-form-label text-warning">Jurusan</label>
            <div class="col-sm-9">
                <select name="jurusan" id="jurusan" class="form-control" onchange="this.form.submit()">
                    <?php $edit->printOption(empty($_POST['jurusan']) ? null : $_POST['jurusan'], ...$edit->jurusan) ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Kelas</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $results['kelas'] ?>" disabled>
            </div>
            <label class="col-sm-3 col-form-label text-warning">Edit Kelas =></label>
            <div class="col-sm-9">
                <select name="kelas" class="form-control" ?>>
                    <?php $edit->printOptKelas($results['kelas'], 'jurusan', $results) ?>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">No. Absen</label>
            <div class="col-sm-9">
                <input type="number" name="no_absen" class="form-control" value="<?php echo $results['no_absen'] ?>" disabled>
            </div>
            <label class="col-sm-3 col-form-label text-warning">Edit No. Absen =></label>
            <div class="col-sm-9">
                <input type="number" name="no_absen" class="form-control" value="<?php echo $results['no_absen'] ?>">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3 col-form-label">Kelompok</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" value="<?php echo $results['kelompok'] ?>" disabled>
            </div>
            <label class="col-sm-3 col-form-label text-warning">Edit Kelompok =></label>
            <div class="col-sm-9">
                <input type="text" name="kelompok" class="form-control" pattern="[A/B]{1}" value="<?php echo $results['kelompok'] ?>">
            </div>
        </div>
        <div class="form-group row col-sm-12 text-center">
            <div class="col-sm-12 text-center">
                <input type="submit" name="edit_submit" value="Edit" class="btn btn-dark px-3">
            </div>
        </div>
    </form>
<?php }
