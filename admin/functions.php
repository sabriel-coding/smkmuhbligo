<?php
include "../config.php";
$connect = new Connection();
$connect = $connect->getConnection();
$func = new Functions();
$jurusan = $func->queryToArray("SELECT jurusan FROM tb_jurusan ORDER BY kode_nis ASC");
$kelas = $func->getKelas();

class Functions {
    var $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'November', 'Desember'];
    var $kelompok = ['A','B'];

    function getKelas() {
        global $jurusan, $_POST, $connect; $kelas = array();
        for ($i=1; $i<=3;$i++) {
            switch ($i) {
                case 1: $subkelas = "X"; break;
                case 2: $subkelas = "XI"; break;
                case 3: $subkelas = "XII"; break;
            }
            if (!empty($_POST['ftr_jurusan'])) {
                $query = mysqli_query($connect, "SELECT kode_nis FROM tb_jurusan WHERE jurusan='{$_POST['ftr_jurusan']}'");
            } else $query = mysqli_query($connect, "SELECT kode_nis FROM tb_jurusan");
            $result = mysqli_fetch_array($query);
            for($j=1;$j<=4;$j++) {
                array_push($kelas, $subkelas.' '.$jurusan[$result[0]-1].' '.$j);
            }
        } return $kelas;
    }
    function getValue($value){
        global $_POST;
        if (!empty($_POST[$value])) return $_POST[$value];
    }
    function printOption($select = null,...$params) {
        foreach($params as $param) {
            if(!empty($select) AND $param == $select) {
                echo "<option value='$param' selected>$param</option>";
                continue;
            } else echo "<option value='$param'>$param</option>";
        }
    }
    function printTh(...$th) {
        foreach($th as $ht) {
            echo "<th>$ht</th>";
        }
    }
    function queryToArray($query) {
        global $connect;
        $array = array();
        $results = mysqli_query($connect,$query);
        while($result = mysqli_fetch_array($results)) {
            $array[] = $result[0];
        } return $array;
    }
    function getQuery($col,$table,...$other) {
        global $_POST;
        $query = "SELECT $col FROM $table WHERE true ";
        if (!empty($_POST['ftr_jurusan'])) {
            $query .= " AND jurusan='{$_POST['ftr_jurusan']}'";
        } if (!empty($_POST['ftr_kelas']) AND !empty($_POST['ftr_jurusan'])) {
            $query .= " AND kelas='{$_POST['ftr_kelas']}'";
        } if(!empty($_POST['ftr_views'])) {
            $query .= " LIMIT 0,{$_POST['ftr_views']}";
        } if (!empty($_POST['ftr_bulan'])) {
            $query .= " AND bulan='{$_POST['ftr_bulan']}'";
        } if (!empty($_POST['ftr_search']) AND empty($other)) {
            $query .= " AND nis LIKE '%{$_POST['ftr_search']}%' OR nama LIKE '%{$_POST['ftr_search']}%'";
        } if (!empty($_POST['ftr_search']) AND !empty($other)) {
            foreach($other as $otheras) {
                $query .= " OR $otheras LIKE '%{$_POST['ftr_search']}%'";
                if($otheras = $other[0]) str_replace('OR','AND',$query);
            }
        }
        return $query;
    }
}

class Absensi extends Functions {
    function getDay() {
        $day = array();
        for ($i = 1; $i <= 31; $i++) {
            // if ($i%6 == 0 OR $i%7 == 0) continue;
            $day[]= $i;
        } return $day;
    }
    function dayToString() {
        $days = $this->getDay();
        $dayo = '';
        $n = 1;
        foreach($days as $dayi) {
            $dayo .= ",`".$dayi."`";
        } return $dayo;
    }
    function getDayFromNum($num) {
        $n = 0; $z = array();
        for($i=1; $i<=31; $i++) {
            $n++;
            if ($n == 8) $n = 1;
            switch ($n) {
                case '1': 
            }
        }
    }
    function printTd($result) {
        $days = $this->getDay();
        foreach($days as $day) {
            if ($result[$day] == null) {$result[$day] = 0;}
            echo "<td>{$result[$day]}</td>";
        }
    }
    function printOptionKelas($select,$type) {
        global $_POST,$kelas;
        if (!empty($_POST[$type])) {
            foreach ($kelas as $kls) {
                if ($kls == $select) {echo "<option value='{$kls}' selected>{$kls}</option>"; continue;}
                echo "<option value='{$kls}'>{$kls}</option>";
            }
        }
    }
}

class TambahForm extends Functions {
    function printValueFromNIS($what) {
        global $_POST, $connect;
        if (!empty($_POST['nis'])) {
            $result = mysqli_query($connect,"SELECT $what FROM tb_siswa WHERE nis=".$_POST['nis']);
            $rest = mysqli_fetch_assoc($result);
            if (mysqli_num_rows($result) == 1) {echo "value='{$rest[$what]}'";}
            else return false;
        }
    }
    function printOptionJurusan() {
        global $_POST, $connect;
        if (!empty($_POST['nis'])) {
            $nis = $_POST['nis'];
            $result = mysqli_query($connect,"SELECT jurusan FROM tb_jurusan WHERE kode_nis=".$nis[0].$nis[1]." LIMIT 1");
            $result = mysqli_fetch_array($result);
            echo "<option value='{$result[0]}'>{$result[0]}</option>";
        }
    }
    function printOptionKelas() {
        global $_POST, $connect;
        $select = $this->getValue('kelas');
        if (!empty($_POST['nis'])) {
            $nis = $_POST['nis'];
            $results = mysqli_query($connect,"SELECT kelas FROM tb_jurusan WHERE kode_nis=".$nis[0].$nis[1]);
            while($result = mysqli_fetch_array($results)) {
                if (isset($select) AND $result[0] == $select) {echo "<option value='{$result[0]}' selected>{$result[0]}</option>";}  
                echo "<option value='{$result[0]}'>{$result[0]}</option>";
            }
        }
    }
    function insertForm($submit,$after,$table) {
        global $_POST, $connect;
        if (isset($_POST[$submit]) AND $table == 'tb_siswa') {
            $query = "INSERT INTO tb_siswa VALUES ({$_POST['nis']},'{$_POST['nama']}','{$_POST['jurusan']}','{$_POST['kelas']}',{$_POST['no_absen']},'{$_POST['kelompok']}')";
            $result = mysqli_query($connect, $query);
            if ($result) {echo "<div class='alert alert-success'>Berhasil Memasukkan Data</div>"; header("refresh:2;url=?page=$after");}
            else echo "<div class='alert alert-danger'>Gagal Memasukkan Data</div>"; header("refresh: 2; url=?page=$after");
        } 
        // elseif (isset($_POST[$submit]) AND $table == 'tb)absensi') {
        //     $query = "INSERT INTO tb_absensi VALUES ({$_POST['nis']},'{$_POST['nama']}','{$_POST['jurusan']}','{$_POST['kelas']}',{$_POST['no_absen']},'{$_POST['kelompok']}')";
        //     $result = mysqli_query($connect, $query);
        //     if ($result) {echo "<div class='alert alert-success'>Berhasil Memasukkan Data</div>"; header("refresh:2;url=?page=$after");}
        //     else echo "<div class='alert alert-danger'>Gagal Memasukkan Data</div>"; header("refresh: 2; url=?page=$after");
        // }
    }
}
class TambahSiswa extends TambahForm {
    function insertTambahSiswa($submit,$after) {
        global $_POST, $connect;
        if (isset($_POST[$submit])) {
            $query = "INSERT INTO tb_siswa VALUES ({$_POST['nis']},'{$_POST['nama']}','{$_POST['jurusan']}','{$_POST['kelas']}',{$_POST['no_absen']},'{$_POST['kelompok']}')";
            $result = mysqli_query($connect, $query);
            $query = "INSERT INTO tb_absensi(nis) VALUES(nis={$_POST['nis']})";
            if ($result) {echo "<div class='alert alert-success'>Berhasil Memasukkan Data</div>"; header("refresh:2;url=?page=$after");}
            else echo "<div class='alert alert-danger'>Gagal Memasukkan Data</div>"; header("refresh: 2; url=?page=$after");
        }
    }
}
class DaftarSiswa extends Functions {
    function printOptionKelas($select,$jurusan) {
        global $connect;
        if (!empty($_POST[$jurusan])) {
            $query = "SELECT kelas FROM tb_jurusan WHERE jurusan='{$_POST[$jurusan]}'";
            $results = mysqli_query($connect, $query);
            while ($result = mysqli_fetch_array($results)) {
                if ($result[0] == $select) {echo "<option value='{$result[0]}' selected>{$result[0]}</option>"; continue;}
                echo "<option value='{$result[0]}'>{$result[0]}</option>";
            }
        }
    }
    function printTh(...$some) {
        foreach($some as $soma) {
            echo "<th>$soma</th>";
        }
    }
    function printTd($page,$results,...$some) {
        while($result = mysqli_fetch_assoc($results)) {
            echo "<tr>";
            foreach($some as $soma) {
                echo "<td>{$result[$soma]}</td>";
            }
            $edit = "<a href='?page=edit&on=$page&action=edit&nis={$result['nis']}' class='btn btn-dark'>Edit</a>";
            $delete = "<a href='?page=edit&on=$page&action=delete&nis={$result['nis']}' class='btn btn-danger'>Delete</a>";
            $print = "<a href='?page=edit&on=$page&action=print&nis={$result['nis']}' class='btn btn-warning'>Print</a>";
            echo "<td class='w-25'><div class='btn-group'>$edit$delete$print</div</td>";
            echo "</tr>";
        }
    }
}
class Actions {
    function checkAction() {
        if (isset($_GET['action'])) {
            switch ($_GET['action']) {
                case 'logout': {
                    $this->logout();
                }
                case 'delete': {
                    if (isset($_GET['nis']) AND isset($_GET['on'])) {
                        $this->delete();
                    }
                }
                case 'edit': {
                    $this->edit();
                }
            }
        }
    }
    function logout() {
        global $_SESSION;
        unset($_SESSION['nis'],$_SESSION['nama'],$_SESSION['jurusan'],$_SESSION['kelas']);
        echo "<div class='alert alert-success'>Terimakasih Telah Berkunjung</div>";
        header("refresh: 2; url=/smkmuhbligo/index.php");
    }
    function delete() {
        global $_GET, $connect;
        $result = mysqli_query($connect,"DELETE FROM {$_GET['on']} WHERE nis={$_GET['nis']}");
        return $result;
    }
    function edit() {
        // header('location: page=edit');
    }
}
class EditForm extends TambahForm {
    function updateForm($submit) {
        global $connect, $_POST;
        if (isset($_POST[$submit])) {
            $query = "UPDATE tb_siswa SET nis={$_POST['nis']},nama='{$_POST['nama']}',jurusan='{$_POST['jurusan']}',kelas='{$_POST['kelas']}',no_absen={$_POST['no_absen']},kelompok='{$_POST['kelompok']}' WHERE nis={$_POST['nis']}";
            $result = mysqli_query($connect, $query);
            return $result ? true : false;
        } 
    }
    function queryToArray($query) {
        global $connect;
        $array = array();
        $results = mysqli_query($connect,$query);
        while($result = mysqli_fetch_array($results)) {
            $array[] = $result[0];
        } return $array;
    }
    function getDataFromNIS() {
        global $connect;
        if(isset($_GET['nis'])) {
            $result = mysqli_query($connect, "SELECT * FROM tb_siswa WHERE nis={$_GET['nis']}");
            return mysqli_fetch_assoc($result);
        }
    }
    function printOptKelas($select,$type,$results) {
        global $connect;
        $query = "SELECT kelas FROM tb_jurusan WHERE jurusan='{$results[$type]}'";
        $results = mysqli_query($connect, $query);
        while ($result = mysqli_fetch_array($results)) {
            if ($result[0] == $select) {echo "<option value='{$result[0]}' selected>{$result[0]}</option>"; continue;}
            echo "<option value='{$result[0]}'>{$result[0]}</option>";
        }
    }
    function printAlert($ambil,$msgscs,$msgerr) {
        if ($ambil == true) {
            echo "<div class='alert alert-success'>$msgscs</div>";
        } else echo "<div class='alert alert-danger'>$msgerr</div>";
    }
}
class LoginToAbsen {
    function loginToAbsen() {
        global $connect;
        $result1 = mysqli_query($connect, "UPDATE tb_absensi SET wush=true WHERE nis={$_GET['nis']}");
        if ($result1) {
            $result2 = mysqli_query($connect,"SELECT tanggal FROM tb_absensi WHERE nis={$_GET['nis']}");
            $result = mysqli_fetch_assoc($result2);
            $day = strtotime($result['tanggal']);
            $result3 = mysqli_query($connect,"INSERT INTO tb_absensi({$day['day']}) VALUES (1)");
            if ($result3) {return true;} else return false;
        }
    }
}