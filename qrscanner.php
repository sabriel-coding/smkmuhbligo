<!-- <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script> -->
<script src="instascan.min.js"></script>
<div class="container-fluid d-flex justify-content-center align-items-center" style="width: 100vw; height: 80vh">
    <div class="container shadow w-auto">
        <form action="" method="post">
            <video id="kamera"></video>
            <script>
                let kamera = document.getElementById('kamera');
                kamera.style.width = "400px";
                kamera.style.height = "400px";
                // kamera.classList.add('bg-white');
                let scanner = new Instascan.Scanner({
                    video: kamera,
                    scanPeriod: 2,
                    mirror: false,
                    refactoryPeriod: 3000,
                    backgroundScan: false
                });
                scanner.addListener('scan', function(content) {
                    window.location.href = "index.php?nis="+content;
                });
                Instascan.Camera.getCameras().then(function(cameras) {
                    if (cameras.length > 0) {
                        scanner.start(cameras[0]);
                        $('[name="options"]').on('change', function() {
                            if ($(this).val() == 1) {
                                if (cameras[0] != "") {
                                    scanner.start(cameras[0]);
                                } else {
                                    alert('Cannot Access Camera!');
                                }
                            } else if ($(this).val() == 2) {
                                if (cameras[1] != "") {
                                    scanner.start(cameras[1]);
                                } else alert('No Back Camera Found!');
                            }
                        })
                    } else {
                        console.error('No Cameras Found!');
                        alert('No Cameras Found!');
                    }
                }).catch(function(e) {
                    console.error(e);
                });
            </script>
        </form>
    </div>
</div>