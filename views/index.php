<?php 
if (empty($_SESSION['nis'])) {
    header('location: ./index.php');
}
$index = new Index();
$profil = new ProfilSekolah();
$other = new VariableUmum();
$index->getDataUser();
include 'header.php';
?>
<div class="jumbotron p-4 bg-white shadow">
    <h2 class="text-center m-0"><?php echo $profil->nama_sekolah ?></h2>
    <h5 class="text-center m-0"><?php echo $other->namaLengkapJurusan[$_SESSION['jurusan']] ?></h5>
</div>