<?php if (isset($_SESSION['nis'])) { ?>
<style>
    @media screen and (max-width: 480px) {
        .img {
            display: none;
        }
    }
    @media screen and (max-width: 388px) {
        .navbar-brand {
            text-align: center;
            margin: 10px auto;
        }
    }
</style>
<header class="navbar bg-dark shadow text-white">
    <div class="container  d-flex justify-content-between align-items-center">
        <div class="p-0 d-flex justify-content-left" style="flex: 1">
            <div class="img bg-white mx-3" style="width: 60px; height: 60px"></div>
            <div class="navbar-brand">
                <h4 class="m-0"><?php echo $_SESSION['nama'] ?></h4>
                <h6 class="m0"><?php echo $_SESSION['kelas'].' / '.$_SESSION['no_absen'] ?></h6>
            </div>
        </div>
        <div class="mb-2 mx-auto">
            <a href="?action=logout" class="btn-sm btn-danger p-2 px-3">Logout</a>
        </div>
    </div>
</header>
<?php }